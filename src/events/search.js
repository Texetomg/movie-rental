const loader = document.querySelector('.loader');
const resultsContainer = document.querySelector('.results__grid');
import { renderSearch } from '../render/renderSearch.js';

const getApi = (searchTerm, page) => {
  return (fetch(
    `http://www.omdbapi.com/?apikey=7ea4aa35&type=movie&s=${searchTerm}&page=${page}`
  ).then((r) => r.json()));
}

export const search = (searchTerm) => {
  localStorage.setItem('search', searchTerm);
  while (resultsContainer.firstChild) {
    resultsContainer.removeChild(resultsContainer.firstChild);
  }
  
  let page = 1;
  let count;
  let totalResults;

  loader.style.visibility = 'visible';
  getApi(searchTerm, page).
  then((Search) => {
    count = Search.totalResults % 10 > 0 ? Math.floor(Search.totalResults / 10) + 1 : Math.floor(Search.totalResults / 10);
    totalResults = Search.totalResults;
    renderSearch(Search.Search, totalResults);
    return Search;
  }).
  then((Search) => {
    for (let i = 2; i <= count; i++) {
      getApi(searchTerm, i)
      .then(r => {
        renderSearch(r.Search, totalResults);
        Search.Search.push(...r.Search);
        i === count ? loader.style.visibility = 'hidden' : 0;
      });    
    }
  });
};
