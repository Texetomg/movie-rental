import { renderSearchTags } from '../render/renderSearchTags.js';
import { SEARCHES, SEARCH } from '../helpers/constants.js';
import { search } from './search.js';

const form = document.querySelector('.search__form');
const input = document.querySelector('.search__input');

export const subscribeToSubmit = () => {
  form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (input.value.toLowerCase() !== localStorage.getItem(SEARCH)) {
      search(input.value);
      let ls = localStorage.getItem(SEARCHES).split(',');
      let setItems = [input.value.toLowerCase()].concat(
        ls.filter((element) => element !== input.value)
      );
      localStorage.setItem(SEARCHES, setItems);
      renderSearchTags();
    }
  });
};
