import { search } from './search.js';
import { renderSearchTags } from '../render/renderSearchTags.js';
import { SEARCHES, SEARCH } from '../helpers/constants.js';

const searchTags = document.querySelector('.search__tags');

const getLocalStorage = () => {
  return (
    localStorage.getItem(SEARCHES).split(',').
    filter((element) => element !== event.target.dataset.movie)
  );
}
export const onTagClick = () => {
  searchTags.addEventListener('click', (event) => {
    event.preventDefault();
    if (
      event.target.classList.contains('search__tag') &&
      !event.altKey &&
      event.target.dataset.movie !== localStorage.getItem(SEARCH)
    ) {
      search(event.target.dataset.movie);
      let ls = getLocalStorage();
      ls.unshift(event.target.dataset.movie);
      localStorage.setItem(SEARCHES, ls);
      renderSearchTags();
    }
  });
};

export const onTagRemove = () => {
  searchTags.addEventListener('click', (event) => {
    event.preventDefault();
    if (event.target.classList.contains('search__tag') && event.altKey) {
      localStorage.setItem(SEARCHES, getLocalStorage());
      renderSearchTags();
    }
  });
};
