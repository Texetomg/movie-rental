import { renderSearchTags } from './render/renderSearchTags.js';
import { onTagClick } from './events/tagEvents.js';
import { onTagRemove } from './events/tagEvents.js';
import { search } from './events/search.js';
import { subscribeToSubmit } from './events/submit.js';
import { SEARCHES, SEARCH } from './helpers/constants.js';
// Components
import './components/currentYear.js';
import './components/movieCard.js';
//init localStorage
if (localStorage.getItem(SEARCHES) == null) localStorage.setItem(SEARCHES, '');
if (localStorage.getItem(SEARCH) == null) localStorage.setItem(SEARCH, '');
//search on page open
search(localStorage.getItem(SEARCH));
renderSearchTags();
//add events
onTagRemove();
onTagClick();
subscribeToSubmit();

const movie = document.querySelector('.results__grid');
movie.addEventListener('mouseover', (event) => {
    event.stopPropagation();
    console.log(event.target.innerHTML);
});