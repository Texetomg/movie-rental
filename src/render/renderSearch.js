import { getDeclension } from '../helpers/getDeclension.js';
import { mapMovie } from '../helpers/mapMovie.js';
import { SEARCHES, SEARCH } from '../helpers/constants.js';

const resultsContainer = document.querySelector('.results__grid');
const resultsHeading = document.querySelector('.results__heading');
const movieCard = document.querySelector('movie-card');
const dMovies = getDeclension('фильм', 'фильма', 'фильмов');

const render = (movieData) => {
  // Используем компонент
  const movie = document.createElement('movie-card');

  // Добавим данные
  movie.poster = movieData.poster;
  movie.title = movieData.title;
  movie.year = movieData.year;
  movie.link = movieData.link;
  movie.imdbID = movieData.imdbID;
  movie.rating = movieData.rating;
  movie.genre = movieData.genre;

  return movie;
};

export const renderSearch = (Search, totalResults) => {
  if (Search !== undefined) {
    const movies = Search.map((result) => render(mapMovie(result)));
    const fragment = document.createDocumentFragment();
    movies.forEach((movie) => {
      return fragment.appendChild(movie);
    });
    resultsContainer.appendChild(fragment);
    resultsHeading.textContent = `Нашли ${totalResults} ${dMovies(totalResults)}`
  } else {
    if (localStorage.getItem(SEARCH) === '' && localStorage.getItem(SEARCHES) === '')
      resultsHeading.textContent = 'Давайте поищем (づ ◕‿◕ )づ';
    else
      resultsHeading.textContent = 'Мы не поняли о чем речь ¯\\_(ツ)_/¯';
  }
};
