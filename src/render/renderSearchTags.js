import { clearNode } from '../helpers/clearNode.js';
import { SEARCHES } from '../helpers/constants.js';

export const renderSearchTags = () => {
  const searchTags = document.querySelector('.search__tags');

  const list = document.createDocumentFragment();
  const ls = localStorage.getItem(SEARCHES);
  if (ls !== null) {
    ls.split(',').forEach((movie) => {
      const tag = document.createElement('a');
      tag.classList.add('search__tag');
      tag.href = `/?search=${movie}`;
      tag.textContent = movie;
      tag.dataset.movie = movie;
      if (tag.textContent !== '') list.appendChild(tag);
    });
    clearNode(searchTags);
    searchTags.appendChild(list);
  }
};
